//
//  ViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/7/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

