//
//  SideBarViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/7/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MapKit
class SideBarViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var contentVuWidth: NSLayoutConstraint!
    var controllerNameStr = ["Home","About Us","Products","Gallery","Qatar Branch","UAE Branch","Contact Us"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentVuWidth.constant = self.view.frame.width-165
        self.revealViewController().rearViewRevealWidth = self.view.frame.width-165
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllerNameStr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        cell.controllerName.text = controllerNameStr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let transition: CATransition = CATransition()
//        let timeFunc : CAMediaTimingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.duration = 0.5
//        transition.timingFunction = timeFunc
//        transition.type = kCATransitionReveal
//        transition.subtype = kCATransitionFromRight
//        self.revealViewController().view.layer.add(transition, forKey: kCATransition)
        
        
        if indexPath.row == 0 {
            let desController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            
            let newFrontController = UINavigationController.init(rootViewController: desController)
//            self.revealViewController().setFront(newFrontController, animated: true)
            self.revealViewController().pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 1 {
            let desController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            
            let newFrontController = UINavigationController.init(rootViewController: desController)
//            self.revealViewController().setFront(newFrontController, animated: true)
            self.revealViewController().pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 2 {
            let desController = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
            
            let newFrontController = UINavigationController.init(rootViewController: desController)
//            self.revealViewController().setFront(newFrontController, animated: true)
            self.revealViewController().pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 3 {
            let desController = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            
            let newFrontController = UINavigationController.init(rootViewController: desController)
//            self.revealViewController().setFront(newFrontController, animated: true)
            self.revealViewController().pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 4 {
            if connectedToNetwork() {
                let desController = self.storyboard?.instantiateViewController(withIdentifier: "WebProductsViewController") as! WebProductsViewController
                
                let newFrontController = UINavigationController.init(rootViewController: desController)
                //            self.revealViewController().setFront(newFrontController, animated: true)
                self.revealViewController().pushFrontViewController(newFrontController, animated: true)
            }else {
                self.openMapForPlace(lat : 25.265584 , long:51.517676)
            }
            
        }else if indexPath.row == 5 {
            if connectedToNetwork() {
                let desController = self.storyboard?.instantiateViewController(withIdentifier: "UAEBranchViewController") as! UAEBranchViewController
                
                let newFrontController = UINavigationController.init(rootViewController: desController)
                //            self.revealViewController().setFront(newFrontController, animated: true)
                self.revealViewController().pushFrontViewController(newFrontController, animated: true)
            }else {
                self.openMapForPlace(lat:24.476225 , long:54.350589)
            }
            
        }else if indexPath.row == 6 {
            let desController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            
            let newFrontController = UINavigationController.init(rootViewController: desController)
//            self.revealViewController().setFront(newFrontController, animated: true)
            self.revealViewController().pushFrontViewController(newFrontController, animated: true)
        }
//        }else if indexPath.row == 7 {
//            PSSTateManager.sharedManager.resetApp()
//            let delegate = UIApplication.shared.delegate as! AppDelegate
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
//
//            delegate.window?.rootViewController = vc
//            UIView.transition(with: delegate.window!, duration: mediumTranstionDuration, options: .transitionFlipFromLeft, animations: {
//            }) { (completed:Bool) in
//            }
//        }
    }
    
    func openMapForPlace(lat:CLLocationDegrees,long:CLLocationDegrees) {
        
        let latitude = lat
        let longitude = long
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "KhairatLebanon Qatar Branch"
        mapItem.openInMaps(launchOptions: options)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
