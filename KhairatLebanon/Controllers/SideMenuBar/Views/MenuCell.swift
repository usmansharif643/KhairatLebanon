//
//  MenuCell.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/8/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var controllerImage: UIImageView!
    @IBOutlet weak var controllerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
