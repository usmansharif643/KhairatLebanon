//
//  ContactUsViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MBProgressHUD

class ContactUsViewController: UIViewController {

    @IBOutlet weak var mesageTV: KMPlaceholderTextView!
    @IBOutlet weak var phoneTxt: PSTextField!
    @IBOutlet weak var emailTxt: PSTextField!
    @IBOutlet weak var nameTxt: PSTextField!
    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func formSubmitAction(_ sender: Any) {
        if let errorMessage = self.getValidationErrorMessage() {
            PopupsHelper.sharedHelper.showPopup("Oops!", content: errorMessage, contentFrame: self.view.frame)
            return
        }
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.setContactUsData(name:self.nameTxt.text!,email:self.emailTxt.text!,phone:self.phoneTxt.text!,message: self.mesageTV.text!){(message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                PopupsHelper.sharedHelper.showPopup("Thanks", content: message!, contentFrame: self.view.frame)
                print("Success")
                
            }else {
                PopupsHelper.sharedHelper.showPopup("Oops!", content: error!.localizedDescription, contentFrame: self.view.frame)
            }
        }
        
    }
    
    func getValidationErrorMessage()->String?{
        var errorMsg = "Please add the missing items!"
        if self.nameTxt.text!.characters.count<1 {
            errorMsg += "\n* Your name required"
        }
        if self.phoneTxt.text!.characters.count<1 {
            errorMsg += "\n* Your phone number required"
        }
        if self.mesageTV.text!.characters.count<1 {
            errorMsg += "\n* Your Description required"
        }
        if self.emailTxt.text!.characters.count<1 {
            errorMsg += "\n* Your Email required"
        }
        return errorMsg == "Please add the missing items!" ? nil : errorMsg
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
