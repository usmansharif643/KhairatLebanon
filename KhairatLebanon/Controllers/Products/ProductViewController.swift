//
//  ProductViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
class ProductViewController: UIViewController,UISearchControllerDelegate, UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var notFoundLbl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    
    var productImages : [String] = [];
    var productsNames : [String] = [];
    var searchWords : [String]? = []
    var productData : [ProductsModel]? = []
    var internetConnectivity : Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        homeTableView.delegate = self
        homeTableView.dataSource = self
        searchBar.delegate = self
        getProducts()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.notFoundLbl.isHidden = (searchWords?.count)! > 1
        return (searchWords?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
        cell.nameLabel.text = searchWords?[indexPath.row]
        if internetConnectivity {
            for var i in 0..<productsNames.count {
                if productsNames[i] == searchWords?[indexPath.row] {
                    cell.productImage.sd_setImage(with: URL(string: productImages[i]), placeholderImage: UIImage(named: "Icon.png"))
                }
            }
        }else {
            for var i in 0..<productsNames.count {
                if productsNames[i] == searchWords?[indexPath.row] {
                    cell.productImage.image = UIImage(named:productImages[i])
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        vc.titlePro = self.searchWords?[indexPath.row]
        vc.imageUrl = self.productData?[indexPath.row].Products?[0].product_image_path
        vc.productID = self.productData?[indexPath.row].Products?[0].product_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchWords = searchText.isEmpty ? productsNames : productsNames.filter {(item:String) -> Bool in
            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        homeTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.searchWords = self.productsNames
        homeTableView.reloadData()
    }
    

    func getProducts(){
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.getProductData(){(response:[ProductsModel]?,message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                self.internetConnectivity = true
                if let resp = response {
                    self.productData = resp
                    for res in resp {
                        self.productsNames.append(res.category_name!)
                        self.productImages.append((res.Products?[0].product_image_path)!)
                    }
                    self.searchWords = self.productsNames
                    self.homeTableView.reloadData()
                }
                
                print("Success")
                //                self.homeTableView.reloadData()
            }else {
                self.internetConnectivity = true
                self.productImages = ["vege_1","vege_2","vege_3","vege_4","fruit_1","fruit_2","fruit_3","fruit_4"];
                self.productsNames = ["vege_1","vege_2","vege_3","vege_4","fruit_1","fruit_2","fruit_3","fruit_4"];
                self.searchWords = self.productsNames
                self.homeTableView.reloadData()
                print("error \(error!.localizedDescription)")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
