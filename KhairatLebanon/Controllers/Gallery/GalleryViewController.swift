//
//  GalleryViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
class GalleryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    var galleryData : [GalleryModel] = []
    var productImages = ["vege_1","vege_2","vege_3","vege_4","fruit_1","fruit_2","fruit_3","fruit_4"];
    var productsNames = ["vege_1","vege_2","vege_3","vege_4","fruit_1","fruit_2","fruit_3","fruit_4"];
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        getGalleryImages()
        homeTableView.delegate = self
        homeTableView.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }
    
    func getGalleryImages(){
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.genericPost(){(response:[GalleryModel]?,message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                self.galleryData = response!
                print("Success")
                self.homeTableView.reloadData()
            }else {
                print("error \(error!.localizedDescription)")
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return galleryData.count
        return productsNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryCell") as! GalleryCell
        cell.nameLabel.text = productsNames[indexPath.row] //galleryData[indexPath.row].imageTitle
//        cell.productImage.sd_setImage(with: URL(string: galleryData[indexPath.row].imagePath!), placeholderImage: UIImage(named: "Icon.png"))
        cell.productImage.image = UIImage(named: productImages[indexPath.row])
        return cell
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
