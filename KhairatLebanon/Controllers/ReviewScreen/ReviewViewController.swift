//
//  ReviewViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/14/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MBProgressHUD

class ReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var ratingTblVu: UITableView!
    @IBOutlet weak var reviewStatus: KMPlaceholderTextView!
    @IBOutlet weak var phonetxt: PSTextField!
    @IBOutlet weak var emailTxt: PSTextField!
    @IBOutlet weak var nameTxt: PSTextField!
    var ratingValue : Int?
    var productID : String?
    var reviewData : [ReviewModel]? = []
    @IBOutlet weak var ratingStars: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let at = PSSTateManager.sharedManager.email {
            self.emailTxt.text = at
        }
        if let at = PSSTateManager.sharedManager.phone {
            self.phonetxt.text = at
        }
        if let at = PSSTateManager.sharedManager.firstName {
            self.nameTxt.text = at
        }
        
//        self.getReviewsList()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (reviewData?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as! ReviewCell
        cell.nameLbl.text = reviewData?[indexPath.row].comments_name
        cell.reviewTxt.text = reviewData?[indexPath.row].comments_review
        cell.ratingLbl.text = (reviewData?[indexPath.row].comments_star)! + "/5"
        return cell
    }
    
    
    @IBAction func RatingAction(_ sender: Any) {
        self.ratingValue = Int(ratingStars.value)
        print("Value \(ratingStars.value)")
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if let errorMessage = self.getValidationErrorMessage() {
            PopupsHelper.sharedHelper.showPopup("Oops!", content: errorMessage, contentFrame: self.view.frame)
            return
        }
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.setProductReview(name:self.nameTxt.text!,email:self.emailTxt.text!,rating:self.ratingValue!,review: self.reviewStatus.text!,productId:productID!){(message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                PopupsHelper.sharedHelper.showPopup("Thanks", content: message!, contentFrame: self.view.frame)
                self.navigationController?.popViewController(animated: true)
                print("Success")
                
            }else {
                PopupsHelper.sharedHelper.showPopup("Oops!", content: error!.localizedDescription, contentFrame: self.view.frame)
            }
        }
    }
    
    func getValidationErrorMessage()->String?{
        var errorMsg = "Please add the missing items!"
        if self.nameTxt.text!.characters.count<1 {
            errorMsg += "\n* Your name required"
        }
        if self.phonetxt.text!.characters.count<1 {
            errorMsg += "\n* Your phone number required"
        }
        if self.reviewStatus.text!.characters.count<1 {
            errorMsg += "\n* Your review description required"
        }
        if self.emailTxt.text!.characters.count<1 {
            errorMsg += "\n* Your Email required"
        }
        if ratingValue == nil {
            errorMsg += "\n* Please set Rating"
        }
        return errorMsg == "Please add the missing items!" ? nil : errorMsg
    }
    
    
    func getReviewsList(){
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.getReviewsLists(productID:self.productID!){(response:[ReviewModel]?,message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                if let reviewsList = response {
                    self.reviewData = reviewsList
                }
                self.ratingTblVu.reloadData()
                print("Success")
                
            }else {
                PopupsHelper.sharedHelper.showPopup("KhairatLebanon", content: "Record Not Found", contentFrame: self.view.frame)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
