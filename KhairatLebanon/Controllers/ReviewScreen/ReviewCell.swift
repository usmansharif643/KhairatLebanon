//
//  ReviewCell.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/15/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

   
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var reviewTxt: PSTextField!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
