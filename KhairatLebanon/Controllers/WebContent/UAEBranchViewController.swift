//
//  UAEBranchViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/28/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MBProgressHUD
import MapKit
class UAEBranchViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    @IBOutlet weak var webView: UIWebView!
    var p : MBProgressHUD?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.title = "UAE Branch"
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.webView.delegate = self
        p = MBProgressHUD.showAdded(to: self.webView, animated: true)
        self.webView.loadRequest(URLRequest(url: URL(string: "https://www.google.com/maps/place/C+Ring+Rd,+Doha,+Qatar/@24.476225,54.350589,16z/data=!4m5!3m4!1s0x3e45c545cc7becd3:0xcbab840f20b71432!8m2!3d25.2630154!4d51.5292616?hl=en-US")!))
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if p != nil {
            p?.hide(animated: true)
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.openMapForPlace()
    }

    
    func openMapForPlace() {
        
        let latitude: CLLocationDegrees = 24.476225
        let longitude: CLLocationDegrees = 54.350589
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "KhairatLebanon Qatar Branch"
        mapItem.openInMaps(launchOptions: options)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
