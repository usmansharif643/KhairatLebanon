//
//  RegistrationViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 11/1/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var saveBtn: PSButtonView!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTxt.delegate = self
        lastNameTxt.delegate = self
        emailTxt.delegate = self
        phoneTxt.delegate = self
        passwordTxt.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    
    @IBAction func saveBtnAction(_ sender: Any) {
        if let errorMessage = self.getValidationErrorMessage() {
            PopupsHelper.sharedHelper.showPopup("Oops!", content: errorMessage, contentFrame: self.view.frame)
            return
        }
        
        let response = DBManager.sharedManager.insertUserInDB(firstName: self.firstNameTxt.text!, lastName: lastNameTxt.text, email: emailTxt.text!, phone: phoneTxt.text!, password: passwordTxt.text!)
        if response {
            PopupsHelper.sharedHelper.showPopup("KhairatLebanon!", content: "User has been Registered", contentFrame: self.view.frame)
            self.navigationController?.popViewController(animated: true)
        }else {
           PopupsHelper.sharedHelper.showPopup("Oops!", content: "Something went wrong!", contentFrame: self.view.frame)
        }
    }
    
    
    
    
    
    func getValidationErrorMessage()->String?{
        var errorMsg = "Please add the missing items!"
        if self.firstNameTxt.text!.characters.count<1 {
            errorMsg += "\n* Your First Name rquired"
        }
        if self.emailTxt.text!.characters.count<1 {
            errorMsg += "\n* Your Email required"
        }
        if self.phoneTxt.text!.characters.count<1 {
            errorMsg += "\n* Your Phone Number required"
        }
        if self.passwordTxt.text!.characters.count<1 {
            errorMsg += "\n* Please set any Password"
        }
        if let email = self.emailTxt.text {
            if !email.isValid() {
                errorMsg += "\n* Email is Invalid formate"
            }
            
        }
        return errorMsg == "Please add the missing items!" ? nil : errorMsg
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
