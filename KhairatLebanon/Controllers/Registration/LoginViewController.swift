//
//  LoginViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 11/1/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxt.delegate = self
        emailTxt.delegate = self
        // Do any additional setup after loading the view.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        if let errorMessage = self.getValidationErrorMessage() {
            PopupsHelper.sharedHelper.showPopup("Oops!", content: errorMessage, contentFrame: self.view.frame)
            return
        }
        
        let response = DBManager.sharedManager.userResponse(email: self.emailTxt.text!, password: passwordTxt.text!)
        if response != nil {
            PSSTateManager.sharedManager.email = response!.email
            PSSTateManager.sharedManager.phone = response!.phone
            PSSTateManager.sharedManager.firstName = response!.firstName
            
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let vc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            delegate.window!.addSubview(vc.view)
            UIView.transition(with: delegate.window!, duration: mediumTranstionDuration, options: .transitionFlipFromRight, animations: {
                
            }) { (completed:Bool) in
                vc.view.removeFromSuperview()
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = vc
            }
        }else {
            PopupsHelper.sharedHelper.showPopup("Oops!", content: "Email or Password in incorrect", contentFrame: self.view.frame)
        }
    }
    
    @IBAction func createAcountAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getValidationErrorMessage()->String?{
        var errorMsg = "Please add the missing items!"
        
        if self.emailTxt.text!.characters.count<1 {
            errorMsg += "\n* Your Email required"
        }
        
        if self.passwordTxt.text!.characters.count<1 {
            errorMsg += "\n* Please set any Password"
        }
        if let email = self.emailTxt.text {
            if !email.isValid() {
                errorMsg += "\n* Email is Invalid formate"
            }
            
        }
        return errorMsg == "Please add the missing items!" ? nil : errorMsg
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
