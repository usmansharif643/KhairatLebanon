//
//  HomeCell.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/7/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
