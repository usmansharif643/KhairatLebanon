//
//  ProductDetailViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/13/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import SDWebImage
class ProductDetailViewController: UIViewController {

    @IBOutlet weak var titleProduct: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    var titlePro : String?
    var imageUrl : String?
    var productID : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleProduct.text = titlePro
        self.productImage.sd_setImage(with: URL(string: imageUrl!), placeholderImage: UIImage(named: "Icon.png"))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var reviewBtn: PSButtonView!
    
    @IBAction func reviewButtonAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
        vc.productID = self.productID!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
