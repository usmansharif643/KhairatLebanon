//
//  AboutUsViewController.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
class AboutUsViewController: UIViewController {

    @IBOutlet weak var barMenuBtn: UIBarButtonItem!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descriptionTv: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#61BB41")
        self.barMenuBtn.target = revealViewController()
        
        self.barMenuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.getAboutUsData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getAboutUsData(){
        let p = MBProgressHUD.showAdded(to: self.view, animated: true)
        PSNetworkManager.sharedNetworkAPIs.getAboutUsData(){(response:AboutUsModel?,message:String?,error:NSError?) in
            p.hide(animated: true)
            if error == nil {
                self.title = response?.aboutTitle
                self.descriptionTv.text = response?.description
                self.image.sd_setImage(with: URL(string: (response?.imagePath)!), placeholderImage: UIImage(named: "Icon.png"))
                print("Success")
            }else {
                print("error \(error!.localizedDescription)")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
