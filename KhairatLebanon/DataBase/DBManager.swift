//
//  DBManager.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/28/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import CoreData

class DBManager: NSObject {
    static let sharedManager = DBManager()
    var managedObjectContext:NSManagedObjectContext!
    override init(){
        super.init()
        managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).getCurrentContext()
    }
    
    func userResponse(email:String,password:String)->Users?{
        let request:NSFetchRequest<Users> = Users.fetchRequest()
        let predi = NSPredicate(format: "email=%@", email)
        request.predicate = predi
        do{
            let resp = try managedObjectContext.fetch(request)
            if resp.count>0{
                if let result = resp.first {
                    if result.password == password {
                        return result
                    }
                }
                
            }
            
        }catch{
            print(error)
        }
        return nil
    }
    
    func insertUserInDB(firstName:String,lastName:String?,email:String,phone:String,password:String) -> Bool{
       
        let resp = NSEntityDescription.insertNewObject(forEntityName: "Users", into: managedObjectContext) as! Users
        resp.firstName = firstName
        resp.email = email
        resp.phone = phone
        resp.password = password
        if let at = lastName {
          resp.lastName = at
        }
        
        let result = saveDB()
        return result
    }
    
    func saveDB () -> Bool {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
                return true
            } catch {
                
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
        
        return false
    }
}
