//
//  ProductType.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/12/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation

public class ProductType {
    public var product_name : String?
    public var product_id : String?
    public var product_image_path : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProductType]
    {
        var models:[ProductType] = []
        for item in array
        {
            models.append(ProductType(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        if let at = dictionary["0"] as? String {
            self.product_id = at
        }
        if let at = dictionary["product_name"] as? String {
            self.product_name = at
        }
        if let at = dictionary["product_image_path"] as? String {
            self.product_image_path = at
        }
    }
}
