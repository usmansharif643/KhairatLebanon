//
//  ProductsModel.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/12/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation
public class ProductsModel {
    public var category_name : String?
    public var category_id : String?
    public var Products : [ProductType]?
    public static var index = 0
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProductsModel]
    {
        var models:[ProductsModel] = []
        for item in array
        {
            models.append(ProductsModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        ProductsModel.index = ProductsModel.index + 1
        if let at = dictionary["category_name"] as? String {
            self.category_name = at
            print("proudcts\(ProductsModel.index - 1)")
        }
        if let at = dictionary["category_id"] as? String {
            self.category_id = at
        }
        
        if (dictionary["proudcts\(ProductsModel.index - 1)"] as? NSArray) != nil  { Products = ProductType.modelsFromDictionaryArray(array: dictionary["proudcts\(ProductsModel.index - 1)"] as! NSArray) }
    }
}
