//
//  ReviewModel.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/15/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation

public class ReviewModel {
    public var comments_star : String?
    public var comments_name : String?
    public var comments_review : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [ReviewModel]
    {
        var models:[ReviewModel] = []
        for item in array
        {
            models.append(ReviewModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        if let at = dictionary["comments_star"] as? String {
            self.comments_star = at
        }
        if let at = dictionary["comments_name"] as? String {
            self.comments_name = at
        }
        if let at = dictionary["comments_review"] as? String {
            self.comments_review = at
        }
    }
}
