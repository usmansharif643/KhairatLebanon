//
//  AboutUsModel.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/10/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation
public class AboutUsModel {
    public var aboutTitle : String?
    public var description : String?
    public var imagePath : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [AboutUsModel]
    {
        var models:[AboutUsModel] = []
        for item in array
        {
            models.append(AboutUsModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    required public init?(dictionary: NSDictionary) {
        if let at = dictionary["about_title"] as? String {
            self.aboutTitle = at
        }
        if let at = dictionary["description"] as? String {
            self.description = at
        }
        if let at = dictionary["filepath"] as? String {
            self.imagePath = at
        }
    }
}
