//
//  PSNetworkManager.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/10/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
import AFNetworking
class PSNetworkManager: NSObject {
     static let sharedNetworkAPIs = PSNetworkManager()
    let afManager = AFHTTPSessionManager()
    var currentTask:URLSessionDataTask?
    override init(){
        super.init()
        let respSer = AFHTTPResponseSerializer()
        respSer.acceptableContentTypes = ["text/html","application/json"]
        afManager.responseSerializer = respSer
        afManager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        afManager.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
    }
    
    func genericPost(completion:@escaping (_ response:[GalleryModel]?,_ message:String,_ error:NSError?)->Void){
        var message = "get gallery Faild"
        var gallery : [GalleryModel]? = []
        currentTask = afManager.get(galleryURL, parameters:nil, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSArray {
                    for t in json as! [NSDictionary] {
                        gallery?.append(GalleryModel(dictionary: t)!)
                    }
                    completion(gallery,message,nil)
                    print("Array of objects \(json)")
                    return
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(nil,message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(nil,error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
    func getAboutUsData(completion:@escaping (_ response:AboutUsModel?,_ message:String,_ error:NSError?)->Void){
        var message = "get gallery Faild"
        currentTask = afManager.get(aboutUs, parameters:nil, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSDictionary, let status = json["status"] as? String {
                    if status == "1" {
                        let data  = AboutUsModel(dictionary: json)
                        completion(data,message,nil)
                        return
                    }
                    
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(nil,message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(nil,error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
    
    func getProductData(completion:@escaping (_ response:[ProductsModel]?,_ message:String,_ error:NSError?)->Void){
        var message = "get gallery Faild"
        var product : [ProductsModel]? = []
        currentTask = afManager.get(productAPI, parameters:nil, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSDictionary{
                    ProductsModel.index = 0
                    for t in json["categories"] as! [NSDictionary] {
                        product?.append(ProductsModel(dictionary: t)!)
                    }
                        completion(product,message,nil)
                        return
                    
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(nil,message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(nil,error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
    
    func setContactUsData(name:String,email:String,phone:String,message:String,completion:@escaping (_ message:String,_ error:NSError?)->Void){
        var message = "get gallery Faild"
        let param : [String:Any] = ["name":name,"phone":phone,"email":email,"message":message]
        currentTask = afManager.get(contactUs, parameters:param, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSDictionary {
                    let msg = json["status"] as? String
                    message = msg!
                    completion(message,nil)
                    return
                    
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
    func setProductReview(name:String,email:String,rating:Int,review:String,productId:String,completion:@escaping (_ message:String,_ error:NSError?)->Void){
        var message = "get gallery Faild"
        let param : [String:Any] = ["name":name,"product_id":productId,"email":email,"message":review,"star":rating]
        currentTask = afManager.get(reviewAPI, parameters:param, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSDictionary {
                    let msg = json["status"] as? String
                    message = msg!
                    completion(message,nil)
                    return
                    
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
    func getReviewsLists(productID:String,completion:@escaping (_ response:[ReviewModel]?,_ message:String,_ error:NSError?)->Void){
        var message = "get Reviews Faild"
        var review : [ReviewModel]? = []
        let param : [String:Any] = ["product_id":productID]
        currentTask = afManager.get(getReviewAPI, parameters:param, progress: nil, success: { (operation, responseObject) in
            do{
                if let json = (try? JSONSerialization.jsonObject(with: responseObject! as! Data, options: JSONSerialization.ReadingOptions(rawValue: 0))) as? NSArray {
                    for t in json as! [NSDictionary] {
                        review?.append(ReviewModel(dictionary: t)!)
                    }
                    completion(review,message,nil)
                    return
                    
                    // do stuff here
                }
                
            }catch{message=error.localizedDescription}
            completion(nil,message,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:message]))
        }, failure: { (task:URLSessionDataTask?, error:Error) in
            completion(nil,error.localizedDescription,  NSError(domain: "APIs", code: 5000, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]))
        })
    }
    
}
