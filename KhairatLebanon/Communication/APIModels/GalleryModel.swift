//
//  GalleryModel.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation

public class GalleryModel {
    public var imageTitle : String?
    public var imagePath : String?
    public class func modelsFromDictionaryArray(array:NSArray) -> [GalleryModel]
    {
        var models:[GalleryModel] = []
        for item in array
        {
            models.append(GalleryModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    required public init?(dictionary: NSDictionary) {
        if let at = dictionary["image_title"] as? String {
            self.imageTitle = at
        }
        if let at = dictionary["image_path"] as? String {
            self.imagePath = at
        }
    }
}
