//
//  PopupsHelper.swift
//  Purple Sheep
//
//  Created by Shmeel Ahmad on 16/09/2016.
//  Copyright © 2016 Shmeel Ahmad. All rights reserved.
//

import UIKit
import KLCPopup
class PopupsHelper: NSObject {
    static let sharedHelper=PopupsHelper()
    
    var popupVC:PopupViewController!
    
    
    func showPopup(_ title:String,content:String,contentFrame:CGRect){
        var errorMessage = content
        var titleStr = title
        if errorMessage == "The Internet connection appears to be offline."{
            errorMessage = "Seems like there is no Internet connection."
            titleStr = "Oops!"
        }else if errorMessage == "Request failed: internal server error (500)" {
            errorMessage = "Something went wrong. Please try again!"
            titleStr = "Oops!"
        }
        self.showPopup(titleStr, content: errorMessage, contentFrame: contentFrame,textAlignment:.center,handler:{}) {}
    }
    
    func showPopup(_ title:String,content:String,contentFrame:CGRect,textAlignment:NSTextAlignment,handler:@escaping ()->Void,closeHandler:@escaping ()->Void){
        self.dismissAll()
        self.popupVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupViewController") as! PopupViewController
        self.popupVC.loadViewIfNeeded()
//        self.popupVC.headerBg.backgroundColor = UIColor.white
        self.popupVC.containerVu.layer.cornerRadius = fieldRoundness
        popupVC.popupTitle.text = title
        popupVC.content.text = content
        popupVC.content.textAlignment = textAlignment
        let pop = KLCPopup(contentView: self.popupVC.view, showType: .bounceIn, dismissType: .bounceOut, maskType: KLCPopupMaskType.dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        pop?.contentView.frame = CGRect(x: 0, y: 0, width: contentFrame.size.width, height: contentFrame.size.height)
        popupVC.handler = handler
        popupVC.closeHandler = closeHandler
        pop?.show()
    }
    func dismissAll(){
        if popupVC != nil {
            popupVC.view.dismissPresentingPopup()
        }
        
    }
}
