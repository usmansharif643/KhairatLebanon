//
//  PopupViewController.swift
//  Purple Sheep
//
//  Created by Shmeel Ahmad on 16/09/2016.
//  Copyright © 2016 Shmeel Ahmad. All rights reserved.
//

import UIKit

class PopupViewController: UIViewController {

    @IBOutlet weak var popupHgt: NSLayoutConstraint!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var containerVu: UIView!
    
    var handler: () -> Void = {}
    var closeHandler: () -> Void = {}
    override func viewDidLoad() {
        super.viewDidLoad()
        containerVu.layer.cornerRadius = fieldRoundness
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var headerBg: UIView!
    @IBAction func closeAction(_ sender: UIButton) {
        closeHandler()
        self.view.dismissPresentingPopup()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popupHgt.constant = min(content.text!.sizeWithConstrainedWidth(width: self.view.frame.size.width-160, font: content.font).height + 110,self.view.frame.size.height-80)
    }
    @IBAction func okPressed(_ sender: UIButton) {
        handler()
        self.view.dismissPresentingPopup()
    }

}
