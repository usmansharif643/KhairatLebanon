//
//  PSSTateManager.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 11/2/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit

class PSSTateManager: NSObject {
    static let sharedManager = PSSTateManager()
    
    
    var email:String? = nil{
        didSet{
            UserDefaults.standard.set(email, forKey: "email")
            UserDefaults.standard.synchronize()
        }
    }
    var phone:String? = nil{
        didSet{
            UserDefaults.standard.set(phone, forKey: "phone")
            UserDefaults.standard.synchronize()
        }
    }
    var firstName:String? = nil{
        didSet{
            UserDefaults.standard.set(firstName, forKey: "firstName")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    override init() {
        if let dt = UserDefaults.standard.object(forKey: "email") as? String {
            email = dt
        }
        if let dt = UserDefaults.standard.object(forKey: "phone") as? String {
            phone = dt
        }
        if let dt = UserDefaults.standard.object(forKey: "firstName") as? String {
            firstName = dt
        }
    }
    
    func resetApp() {
        email = nil
        phone = nil
        firstName = nil
    }
}
