//
//  Constant.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/9/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation

let fieldRoundness:CGFloat = 8.0
let mediumTranstionDuration = 0.4



let galleryURL = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=gallery"
let aboutUs = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=aboutUs"

let productAPI = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=products"

let contactUs = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=contactEmail"

let reviewAPI = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=rating"
let getReviewAPI = "http://www.khairatlebanon.com/webservices/webservice.php?service_type=reviews"
