//
//  KMPlaceholderTextView.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/11/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class KMPlaceholderTextView: UITextView {
    @IBInspectable var psBorderColor:UIColor = UIColor.clear{
        didSet{
            layer.borderColor = psBorderColor.cgColor
        }
    }
    @IBInspectable var psBorderWidth:CGFloat = 1.0{
        didSet{
            layer.borderWidth = psBorderWidth
        }
    }
    @IBInspectable var psCornerRadius:CGFloat = fieldRoundness{
        didSet{
            layer.cornerRadius = psCornerRadius
        }
    }
}
