//
//  PSButtonView.swift
//  KhairatLebanon
//
//  Created by Muhammad Usman on 10/11/17.
//  Copyright © 2017 Muhammad Usman. All rights reserved.
//

import UIKit
@IBDesignable
class PSButtonView: UIButton {
    @IBInspectable var psBorderColor:UIColor = UIColor.clear{
        didSet{
            layer.borderColor = psBorderColor.cgColor
        }
    }
    @IBInspectable var psBorderWidth:CGFloat = 1.0{
        didSet{
            layer.borderWidth = psBorderWidth
        }
    }
    @IBInspectable var psCornerRadius:CGFloat = fieldRoundness{
        didSet{
            layer.cornerRadius = psCornerRadius
        }
    }
    @IBInspectable var psContentMode:UIViewContentMode = .scaleAspectFill{
        didSet{
            self.imageView?.contentMode = psContentMode
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel?.minimumScaleFactor = 0.7
        titleLabel?.numberOfLines=2
        titleLabel?.adjustsFontSizeToFitWidth = true
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        titleLabel?.minimumScaleFactor = 0.7
        titleLabel?.numberOfLines=2
        titleLabel?.adjustsFontSizeToFitWidth = true
    }
}
